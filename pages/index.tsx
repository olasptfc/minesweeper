import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Minesweeper from '../components/Minesweeper'
import { useState } from 'react'
import { Difficulties } from '../types'

export default function Home(): JSX.Element {
  const [playing, setPlaying] = useState<boolean>(false)
  const [gameOver, setGameOver] = useState<boolean>(false)
  const [difficulty, setDifficulty] = useState<Difficulties>('medium')
  
  const handleGameOver = () => {
    setPlaying(false)
    setGameOver(true)
  }

  const handleDifficultyChange = (event) => {
    setDifficulty(event.target.value)
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Minesweeper</title>
        <meta name="description" content="Take Home Minesweeper App - Billy McIntosh" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1>Minesweeper</h1>
        {gameOver && 'Game Over!'}
        <div>
          Difficulty:
          <select onChange={handleDifficultyChange} name="difficulty">
            <option value="easy">Easy</option>
            <option value="medium">Medium</option>
            <option value="hard">Hard</option>
          </select>
          <button onClick={() => setPlaying(!playing)} type="button">{playing ? 'Restart' : ' Start'}</button>
        </div>

        {playing && <Minesweeper difficulty={difficulty} cols={5} rows={5} bombs={5} handleGameOver={handleGameOver} />}
      </main>
    </div>
  )
}
