import { CellState, GridCell, BombsIndex, IGameProps, IRange, Grid, IMinesweeper } from './../../types/index'

// adds bombs, fills remainder of cells to array then randomises
const createBombsIndex = (bombs: number, cells: number): BombsIndex[] => {
  return [...Array(cells - bombs).fill(CellState.PROXIMITY_0), ...Array(bombs).fill(CellState.BOMB)].sort(
    () => Math.random() - 0.5
  )
}

// returns up to 3x3 grid coords from input coords
const getAdjacentCellsFromCoords = (props: IGameProps): IRange => {
  const { x, y, rows, cols } = props
  const maxCols = cols - 1
  const maxRows = rows - 1

  return {
    start: {
      x: x - 1 < 0 ? 0 : x - 1,
      y: y - 1 < 0 ? 0 : y - 1
    },
    end: {
      x: x + 1 > maxRows ? maxRows : x + 1,
      y: y + 1 > maxCols ? maxCols : y + 1
    }
  }
}

const inRange = (item: GridCell, range: IRange): boolean => {
  return item.x >= range.start.x &&
    item.y >= range.start.y &&
    item.x <= range.end.x &&
    item.y <= range.end.y
}

// counts number of bombs in 3x3 proximity of coords
const bombsInProximity = (grid: Grid, range: IRange): number => {
  return grid
    .flat()
    .filter(item => inRange(item, range) && item.value === CellState.BOMB).length
}

const updateCell = (props): Grid => {
  const {
    grid,
    value,
    x,
    y,
  } = props

  grid[x][y] = { x, y, value, isVisible: true }

  return grid.slice()
}

const traverseCells = (props: IGameProps): Grid => {
  const range = getAdjacentCellsFromCoords(props)
  let grid = props.grid

  grid
    .flat()
    .filter(item => inRange(item, range) && item.value !== CellState.CLEAR)
    .forEach(cell => {
      if (cell.value === CellState.PROXIMITY_0) {
        grid = updateCell({ grid, x: cell.x, y: cell.y, value: CellState.CLEAR })

        traverseCells({
          grid,
          cols: props.cols,
          rows: props.rows,
          x: cell.x,
          y: cell.y
        })
      } else if (cell.value !== CellState.BOMB) {
        grid = updateCell({ x: cell.x, y: cell.y, grid, value: cell.value })
      }
    })

  
  return grid.slice()
}


// generates grid with bombs and proximity data
const minesweeperGrid = (props: IMinesweeper): Grid | unknown[] => {
  const { bombs, cols, rows } = props

  const bombsIdx = createBombsIndex(bombs, cols * rows)

  const grid = []
  for (let x = 0; x < rows; x += 1) {
    grid.push(
      Array.from({ length: cols }, (_, y) => {
        return { x, y, isVisible: false, value: bombsIdx.pop() }
      })
    )
  }

  const populated = grid.flat().map((item) => {
    const range = getAdjacentCellsFromCoords({
      x: item.x,
      y: item.y,
      rows,
      cols
    })

    const proximity = bombsInProximity(grid, range)
    const value = item.value === CellState.BOMB ? CellState.BOMB : CellState[`PROXIMITY_${proximity}`]
    return { ...item, value }
  })

  // chunk back into rows & cols
  return Array.from({ length: Math.ceil(populated.length / rows) }, (_, i) =>
    populated.slice(i * rows, i * rows + rows)
  )
}

export {
  minesweeperGrid,
  updateCell,
  traverseCells,
}
