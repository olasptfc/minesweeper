import { useState } from 'react'
import { CellState, MinesweeperProps } from '../../types'
import { minesweeperGrid, traverseCells, updateCell } from './components'

import difficulties from './difficulties.json'
import styles from './Minesweeper.module.scss'

export default function Minesweeper(props: MinesweeperProps): JSX.Element {
  const {
    bombs,
    cols,
    rows,
  } = props.difficulty ? difficulties[props.difficulty] : props

  const handleGameOver = props.handleGameOver

  const [grid, setGrid] = useState(minesweeperGrid({ bombs, cols, rows }))

  const handleCellClick = (props) => {
    const {
      x,
      y,
      value
    } = props

    if (value === CellState.BOMB) {
      handleGameOver(true)
    } else if (value > 0) {
      const updatedGrid = updateCell({ x, y, grid })
      setGrid(updatedGrid)
    } else {
      const updatedGrid = traverseCells({ x, y, rows, cols, grid })
      setGrid(updatedGrid)
    }
  }

  return (
    <div data-testid="game" className={styles.container}>
      {grid.map((item, rIdx) => (
        <div key={rIdx} className="cellRow">
          {item.map((cell, cIdx) => cell.isVisible ? (
            <div
              key={cIdx}
              className={`${styles.cell} ${styles.cellSafe} ${styles[`proximity-${cell.value}`]}`}
            >
              {cell.value !== CellState.CLEAR ? cell.value : ''}
            </div>
          ) : (
            <button
              onClick={() => handleCellClick({ ...cell })}
              key={cIdx}
              className={`${styles.cell} ${cell.value === CellState.CLEAR ? styles.safe : ''}`}
            />
          )
          )}
        </div>
      ))}
    </div>
  )
}
