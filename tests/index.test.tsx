import { fireEvent, render, screen } from '@testing-library/react'
import App from '../pages/index'

describe('App', () => {
  it('renders without crashing', () => {
    render(<App />)
    expect(
      screen.getByRole('heading', { name: 'Minesweeper' })
    ).toBeInTheDocument()
  })

  it('click button to start new game', () => {
    render(<App />)

    const startGameButton = screen.getByRole('button')
    fireEvent.click(startGameButton)

    const gameCanvas = screen.getByTestId('game')
    expect(gameCanvas).toBeVisible()
  })
})