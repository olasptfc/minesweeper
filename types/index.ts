
export type Difficulties = 'easy' | 'medium' | 'hard'

export enum CellState {
  CLEAR = -1,
  BOMB = '💣',
  PROXIMITY_0 = 0,
  PROXIMITY_1 = '1️⃣',
  PROXIMITY_2 = '2️⃣',
  PROXIMITY_3 = '3️⃣',
  PROXIMITY_4 = '4️⃣',
  PROXIMITY_5 = '5️⃣',
  PROXIMITY_6 = '6️⃣',
  PROXIMITY_7 = '7️⃣',
  PROXIMITY_8 = '8️⃣',
}

export type MinesweeperProps = {
  difficulty?: Difficulties
  cols?: number
  rows?: number
  bombs?: number
  handleGameOver: React.Dispatch<React.SetStateAction<boolean>>
}

export interface IRange {
  start: IStartOrEnd;
  end: IStartOrEnd;
}

interface IStartOrEnd {
  x: number;
  y: number;
}

export interface IGameProps {
  x: number;
  y: number;
  rows: number;
  cols: number;
  grid?: Grid;
}

export interface GridCell {
  x: number;
  y: number;
  value: number | string;
  visible: boolean;
}

export interface IMinesweeper {
  bombs: number;
  cols: number;
  rows: number;
}

export type Grid = (GridCell)[] | null;

export type BombsIndex = string | number