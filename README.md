# README.md - Billy McIntosh, Minesweeper Task

## Install

- Clone repo: 
- `npm install / yarn`
- `npm run dev / yarn dev`

## Testing

- I added some rudimentary tests; 
- `npm run test`

## Intro

- Fun task, made me aim for performance over presentation.  
- I had a look at some implementations in multiple languages and wanted to do it a bit differently with the reasonably newer javascript constructs.

## Performance

- Recursive methods are rarely performant but it runs reasonably well.  It has nothing on Windows Minesweeper or other native implementations.
- Using `for` may have been more performant due to the overhead of the array methods I used

## Improvements

- Have a `game won` function
- Perfect the TypeScript types
- React implementation could be improved
- I'd probably change the cell items to a `Map()` with an index for easier manipulation and eradicate the side-effects created by mutating the array directly. 
- I did use Immer to curtail the side-effects but even though it's a 3k library, it was overkill and I hacked around it by slicing the returned array so it appeared to be immutable.
- Better styling!
- Flags
- A timer could have been added

